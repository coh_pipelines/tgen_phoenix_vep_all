FROM localhost:5000/ubuntu_opt_bcftools:1.10.2 as opt_bcftools

FROM localhost:5000/ubuntu_base:19.04

RUN apt update -y && \
    apt install -y unzip curl git libmodule-build-perl libdbi-perl libdbd-mysql-perl build-essential zlib1g-dev libbz2-dev liblzma-dev && \
    apt autoremove -y && \
    apt clean

COPY --from=opt_bcftools /opt /opt

RUN mkdir -p /opt && \
    cd /opt/ && git clone https://github.com/Ensembl/ensembl-vep.git && \
    cd /opt/ensembl-vep/ && ./INSTALL.pl -a a && \
    cd /opt/ensembl-vep/ && mkdir -p /opt/bin && cp ./vep /opt/bin/

RUN apt remove -y unzip curl git build-essential zlib1g-dev && \
    apt autoremove -y && \
    apt clean

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/
ENV PERL5LIB /opt/ensembl-vep/modules/:/opt/ensembl-vep/

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module
